# moxa_faker

This little utility creates a programming sequence for SFP-DACs with standard 24LC02 EEPROMs.
It will generate a Moxa compatible recognition string inside the vendor specific EEPROM area, 
and also include faked values for the online monitoring (which is not included in SFP-DACs, and
correctly noted in standard EEPROM, but kindly ignored by the MOXA firmware).
Only the checksum for standard EEPROM area (0xa0) is generated, the online monitoring stuff 
(0xa2) is just more or less ignored. 

Tested with Moxa EDS G516E and Moxa EDS G509 switches.

Anyway, if you have to option to avoid Moxa switches by design, this definitely is the way to go.

The script generated may or may not work on SFPs. Modern SFPs include usually one ASIC with a 
special programming sequence, which usually is "super duper high security IP" of the SFP vendor
(if you are from WayStream, no, I don't mean WayStream here specifically).
If you dare, try it, but don't touch the 0xa2 EEPROM in that case.

You have been warned.
