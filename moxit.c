/* includes */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* prototypes */

/* defines */

/***********************************************************************************/
/***********************************************************************************/
/* main() */
/***********************************************************************************/
/***********************************************************************************/
int main(int argc, char** argv)
{
  uint8_t  result = 0;
  uint8_t  rom[256];
  uint16_t i;
  uint16_t crc;
  uint32_t serial = 0;
  char sn_string[9];

  if( (argc < 2) ) {
    printf("Error: need serial number as argument\n");
    exit(1);
  }

  serial = (uint32_t)strtoul(argv[1], NULL, 0);

  if( serial > 9999 ) {
    printf("Error: serial number too large\n");
    exit(1);
  }

  snprintf(sn_string, sizeof(sn_string), "%08u", serial);

//  printf("Info: S/N %u \"%s\"\n", serial, sn_string);

  /************************** ROM 0x50 *************************/
  /* clear rom */
  for( i = 0; i < 256; i++ ) {
    rom[i] = 0;
  }
    
  /* setup the information */
  rom[0]   = 0x03; /* type of transceiver */
  rom[1]   = 0x04;
  rom[2]   = 0x07; /* connector */
  rom[6]   = 0x01;
  rom[7]   = 0x40;
  rom[9]   = 0x0c;
  rom[11]  = 0x01; /* encoding */
  rom[12]  = 0x0d; /* bitrate */
  rom[16]  = 0xc8; /* length */
  rom[17]  = 0x64; /* length */
  rom[20]  = 'M'; /* vendor name */
  rom[21]  = 'o'; /* vendor name */ 
  rom[22]  = 'x'; /* vendor name */
  rom[23]  = 'a'; /* vendor name */
  rom[24]  = 'n'; /* vendor name */
  rom[25]  = 'e'; /* vendor name */
  rom[26]  = 't'; /* vendor name */
  rom[27]  = ' '; /* vendor name */
  rom[28]  = ' '; /* vendor name */
  rom[29]  = ' '; /* vendor name */
  rom[30]  = ' '; /* vendor name */
  rom[31]  = ' '; /* vendor name */
  rom[32]  = ' '; /* vendor name */
  rom[33]  = ' '; /* vendor name */
  rom[34]  = ' '; /* vendor name */
  rom[35]  = ' '; /* vendor name */
  rom[40]  = 'S'; /* vendor PN */
  rom[41]  = 'F'; /* vendor PN */
  rom[42]  = 'P'; /* vendor PN */
  rom[43]  = '-'; /* vendor PN */
  rom[44]  = '1'; /* vendor PN */
  rom[45]  = 'G'; /* vendor PN */
  rom[46]  = '2'; /* vendor PN */
  rom[47]  = '0'; /* vendor PN */
  rom[48]  = 'B'; /* vendor PN */
  rom[49]  = 'L'; /* vendor PN */
  rom[50]  = 'C'; /* vendor PN */
  rom[51]  = '-'; /* vendor PN */
  rom[52]  = 'T'; /* vendor PN */
  rom[53]  = ' '; /* vendor PN */
  rom[54]  = ' '; /* vendor PN */
  rom[55]  = ' '; /* vendor PN */
  rom[56]  = ' '; /* revision */
  rom[57]  = ' '; /* revision */
  rom[58]  = ' '; /* revision */
  rom[59]  = ' '; /* revision */
  rom[60]  = 0x05; /* wave length */
  rom[61]  = 0x1e; /* wave length */
  rom[63]  = 0xff; /*** CRC ***/
  rom[65]  = 0x1a;
  rom[68]  = 'S'; /* serial number */
  rom[69]  = 'u'; /* serial number */
  rom[70]  = 'X'; /* serial number */
  rom[71]  = sn_string[4]; /* serial number */
  rom[72]  = sn_string[5]; /* serial number */
  rom[73]  = sn_string[6]; /* serial number */
  rom[74]  = sn_string[7]; /* serial number */
  rom[75]  = ' '; /* serial number */
  rom[76]  = ' '; /* serial number */
  rom[77]  = ' '; /* serial number */
  rom[78]  = ' '; /* serial number */
  rom[79]  = ' '; /* serial number */
  rom[80]  = ' '; /* serial number */
  rom[81]  = ' '; /* serial number */
  rom[82]  = ' '; /* serial number */
  rom[83]  = ' '; /* serial number */
  rom[84]  = '2'; /* date code */
  rom[85]  = '0'; /* date code */
  rom[86]  = '2'; /* date code */
  rom[87]  = '3'; /* date code */
  rom[88]  = '0'; /* date code */
  rom[89]  = '4'; /* date code */
  rom[90]  = '0'; /* date code */
  rom[91]  = '1'; /* date code */
  rom[92]  = 0x68;
  rom[93]  = 0xf0;
  rom[94]  = 0x01;
  rom[95]  = 0xff; /*** CRC ***/
  rom[96]  = 'M'; /* vendor specific */
  rom[97]  = 'o'; /* vendor specific */
  rom[98]  = 'x'; /* vendor specific */
  rom[99]  = 'a'; /* vendor specific */
  rom[100] = 'n'; /* vendor specific */
  rom[101] = 'e'; /* vendor specific */
  rom[102] = 't'; /* vendor specific */
  rom[103] = ' '; /* vendor specific */
  rom[104] = 'S'; /* vendor specific */
  rom[105] = 'F'; /* vendor specific */
  rom[106] = 'P'; /* vendor specific */
  rom[107] = '-'; /* vendor specific */
  rom[108] = '1'; /* vendor specific */
  rom[109] = 'G'; /* vendor specific */
  rom[110] = '2'; /* vendor specific */
  rom[111] = '0'; /* vendor specific */
  rom[112] = 'B'; /* vendor specific */
  rom[113] = 'L'; /* vendor specific */
  rom[114] = 'C'; /* vendor specific */
  rom[115] = '-'; /* vendor specific */
  rom[116] = 'T'; /* vendor specific */
  rom[117] = ' '; /* vendor specific */
  rom[118] = ' '; /* vendor specific */
  rom[119] = ' '; /* vendor specific */
  rom[120] = ' '; /* vendor specific */
  rom[121] = ' '; /* vendor specific */
  rom[122] = ' '; /* vendor specific */
  rom[123] = ' '; /* vendor specific */
  rom[124] = ' '; /* vendor specific */
  rom[125] = ' '; /* vendor specific */
  rom[126] = ' '; /* vendor specific */
  rom[127] = ' '; /* vendor specific */
  
  /* base CRC */
  crc = 0;
  for( i = 0; i < 63; i++ ) {
    crc += rom[i];
  }
  rom[63] = (uint8_t)(crc & 0x000000ff);
    
  /* extended CRC */
  crc = 0;
  for( i = 64; i < 95; i++ ) {
    crc += rom[i];
  }
  rom[95] = (uint8_t)(crc & 0x000000ff);

  printf("#! /bin/bash\n");
  for( i = 0; i < 256; i += 8 ) {
    printf("i2ctransfer -y 0 w9@0x50 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\n", 
           (uint8_t)i, rom[i], rom[i+1], rom[i+2], rom[i+3], rom[i+4], rom[i+5], rom[i+6], rom[i+7]);
  }

  /************************** ROM 0x51 *************************/
  /* clear rom */
  for( i = 0; i < 256; i++ ) {
    rom[i] = 0xff;
  }
  
  /* setup the information */
  rom[96]  = 0x21; /* T MSB       */ /* 33.333 C */
  rom[97]  = 0x55; /* T LSB       */
  rom[98]  = 0x82; /* Vcc MSB     */ /* 3.3333 V */
  rom[99]  = 0x25; /* Vcc LSB     */
  rom[100] = 0x82; /* TX Bias MSB */ /**/ 
  rom[101] = 0x35; /* TX Bias LSB */
  rom[102] = 0x08; /* TX Pwr MSB  */ /* -6.6 dBm */
  rom[103] = 0x43; /* TX Pwr LSB  */
  rom[104] = 0xb5; /* RX Pwr MSB  */ /*  6.6 dBm */
  rom[105] = 0x4f; /* RX Pwr LSB  */

  /* values for TX/RX power are stored in 0.1uW, but shown as dBm in the switch */

  printf("\n");
  printf("i2ctransfer -y 0 w9@0x51 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\n", 
         96, rom[96], rom[97], rom[98], rom[99], rom[100], rom[101], rom[102], rom[103]);
  printf("i2ctransfer -y 0 w3@0x51 0x%02x 0x%02x 0x%02x\n", 
         104, rom[104], rom[105]);
    
  return 0;
}



